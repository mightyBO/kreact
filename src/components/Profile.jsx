import React from 'react';
import { PulseLoader } from 'react-spinners';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Icon from 'material-ui/Icon';
import auth from '../services/auth';
import misc from '../services/misc';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      error: '',
      loading: true,
    };
  }

  async componentDidMount() {
    const result = await auth.getProfile();
    const error = misc.getError(result);

    /* eslint-disable-next-line */
    this.setState({
      error,
      loading: false,
      profile: result.data || {},
    });
  }

  render() {
    return !this.state.loading ? (
      <div>
        <List>
          <ListItem>
            <ListItemIcon>
              <Icon>email</Icon>
            </ListItemIcon>
            <ListItemText primary="Login" secondary={this.state.profile.email} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon>accessible</Icon>
            </ListItemIcon>
            <ListItemText primary="Account type" secondary={this.state.profile.accountType} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon>check_circle</Icon>
            </ListItemIcon>
            <ListItemText
              primary="Verified"
              secondary={misc.getBoolean(this.state.profile.isVerifiedEmail)}
            />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon>fast_forward</Icon>
            </ListItemIcon>
            <ListItemText
              primary="Use direct links"
              secondary={misc.getBoolean(this.state.profile.useDirectLinks)}
            />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <Icon>lock</Icon>
            </ListItemIcon>
            <ListItemText
              primary="Force SSL"
              secondary={misc.getBoolean(this.state.profile.forceSsl)}
            />
          </ListItem>
        </List>
        <div className="error-label">{this.state.error}</div>
      </div>
    ) : (
      <div className="spinner-container">
        <PulseLoader color="#ffe082" loading={this.state.loading} />
      </div>
    );
  }
}

export default Profile;
