import React from 'react';
import { PulseLoader } from 'react-spinners';
import List, { ListItem, ListItemText } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import misc from '../services/misc';
import file from '../services/file';

class FileList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      error: '',
      loading: true,
    };
  }

  async componentDidMount() {
    const result = await file.getAllFiles();
    const error = misc.getError(result);

    /* eslint-disable-next-line */
    this.setState({
      error,
      loading: false,
      items: result.data.items || [],
    });
  }

  render() {
    return (
      <div>
        <FileItems items={this.state.items} />
        <div className="spinner-container">
          <PulseLoader color="#ffe082" loading={this.state.loading} />
        </div>
        <div className="error-label">{this.state.error}</div>
      </div>
    );
  }
}

const FileItem = props => (
  <ListItem>
    <IconButton color="inherit">
      <Icon color="primary">cloud_download</Icon>
    </IconButton>
    <ListItemText primary={props.item.name} secondary={misc.getSize(props.item.size)} />
  </ListItem>
);

const FileItems = props => (
  <List>{props.items.map(item => <FileItem item={item} key={item.id} />)}</List>
);

export default FileList;
