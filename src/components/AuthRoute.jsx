import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import auth from '../services/auth';

const AuthRoute = props => (auth.user.authenticated ? (
  <Route {...props} />
) : (
  <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
));

export default AuthRoute;
