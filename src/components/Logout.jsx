import React from 'react';
import { Redirect } from 'react-router-dom';
import auth from '../services/auth';

class Logout extends React.Component {
  constructor(props) {
    super(props);

    localStorage.removeItem('token');
    auth.user.authenticated = false;
  }

  render() {
    return (
      <Redirect to="/login" />
    );
  }
}

export default Logout;
