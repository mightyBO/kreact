import React from 'react';
import { Redirect } from 'react-router-dom';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { PulseLoader } from 'react-spinners';
import auth from '../services/auth';
import misc from '../services/misc';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      error: '',
      loading: false,
      redirectTo: '',
    };

    this.handleChangeLogin = this.handleChangeLogin.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeLogin(event) {
    this.setState({ login: event.target.value });
  }

  handleChangePassword(event) {
    this.setState({ password: event.target.value });
  }

  async handleSubmit(event) {
    this.setState({ loading: true });
    const result = await auth.signIn(this.state.login, this.state.password);
    const error = misc.getError(result);
    this.setState({ error, loading: false });
    if (!error) {
      this.setState({
        redirectTo: '/' || (this.props.location.state && this.props.location.state.from.pathname),
      });
    }
    event.persist();
  }

  render() {
    if (this.state.redirectTo) {
      return <Redirect to={this.state.redirectTo} />;
    }
    return (
      <div>
        <div className="height-30" />
        <TextField label="Login" value={this.state.login} onChange={this.handleChangeLogin} />
        <br />
        <TextField
          label="Password"
          value={this.state.password}
          onChange={this.handleChangePassword}
          type="password"
        />
        <br />
        <div className="btn-wide">
          <Button variant="raised" color="primary" label="Sign in" onClick={this.handleSubmit}>
            Sign in
          </Button>
        </div>
        <div className="spinner-container">
          <PulseLoader color="#ffe082" loading={this.state.loading} />
        </div>
        <div className="error-label">{this.state.error}</div>
      </div>
    );
  }
}

export default Login;
