import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import '../css/App.css';
import Login from './Login';
import Logout from './Logout';
import Profile from './Profile';
import AuthRoute from './AuthRoute';
import FileList from './FileList';
import ButtonAppBar from './ButtonAppBar';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#ffffb3',
      main: '#ffe082',
      dark: '#caae53',
      contrastText: '#000000',
    },
    secondary: {
      light: '#fffffb',
      main: '#dcedc8',
      dark: '#aabb97',
      contrastText: '#000000',
    },
  },
});

const App = () => (
  <MuiThemeProvider theme={theme}>
    <div className="App">
      <Router>
        <div>
          <ButtonAppBar />
          <Switch>
            <AuthRoute exact path="/" component={FileList} />
            <AuthRoute exact path="/profile" component={Profile} />
            <Route exact path="/login" component={Login} />
            <AuthRoute exact path="/logout" component={Logout} />
          </Switch>
        </div>
      </Router>
    </div>
  </MuiThemeProvider>
);

export default App;
