import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './css/index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import auth from './services/auth';
import config from './config';

axios.defaults.baseURL = config.baseUrl;

const token = localStorage.getItem('token');
if (token) {
  auth.applyToken(token);
}
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
