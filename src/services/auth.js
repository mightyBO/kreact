import axios from 'axios';
import config from '../config';

const user = {
  authenticated: false,
};

const applyToken = (token) => {
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    user.authenticated = true;
  }
};

const getProfile = async () => {
  try {
    const response = await axios.get('/users/me');
    return response;
  } catch (err) {
    return err;
  }
};

const signIn = async (login, password) => {
  try {
    const response = await axios.post('/auth/token', {
      client_id: config.clientId,
      client_secret: config.clientSecret,
      grant_type: 'password',
      username: login,
      password,
    });

    applyToken(response.data.access_token);
    localStorage.setItem('token', response.data.access_token);

    return response;
  } catch (err) {
    return err;
  }
};

export default {
  signIn,
  applyToken,
  user,
  getProfile,
};
