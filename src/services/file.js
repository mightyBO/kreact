import axios from 'axios';

const getAllFiles = async () => {
  try {
    const response = await axios.get('/files?limit=20&offset=0');
    return response;
  } catch (err) {
    return err;
  }
};

export default {
  getAllFiles,
};
