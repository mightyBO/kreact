import filesize from 'filesize';

const getError = (result) => {
  if (result instanceof Error) {
    if (result.response && result.response.data.error_description) {
      return result.response.data.error_description;
    }
    return result.message;
  }
  return null;
};

const getSize = size => filesize(size);

const getBoolean = boolean => (boolean ? 'Yes' : 'No');

export default {
  getError,
  getSize,
  getBoolean,
};
